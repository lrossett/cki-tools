import copy

from pipeline_tools import utils


def main(project, args):
    variable_filter = copy.copy(args.variable_filter)
    if args.cki_pipeline_type:
        variable_filter['cki_pipeline_type'] = args.cki_pipeline_type
    return utils.get_last_successful_pipeline(project,
                                              args.cki_pipeline_branch,
                                              variable_filter=variable_filter)
