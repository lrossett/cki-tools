"""Gitlab api interaction tests."""
import hashlib
import hmac
import json
import os
import unittest
from unittest import mock

import pika

from cki.cki_tools.webhook_receiver import flask
from cki.cki_tools.webhook_receiver import receiver

SECRET = 'secret'


@mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': SECRET,
                              'IS_PRODUCTION': 'True'})
class TestWebhook(unittest.TestCase):
    """ Test webhook receiver."""

    def setUp(self):
        """SetUp class."""
        self.client = flask.app.test_client()

    def _check_send(self, event, data):
        response = self.client.post(
            '/', json=data, headers={'X-Gitlab-Token': SECRET,
                                     'X-Gitlab-Event': event})
        self.assertEqual(response.status, '200 OK')

    @staticmethod
    def _publish_call(event, data, routing_key):
        properties = pika.BasicProperties(
            delivery_mode=2,
            headers={
                'webhook-type': 'gitlab',
                'webhook-gitlab-event': event})
        return mock.call(
            exchange=receiver.RABBITMQ_EXCHANGE,
            routing_key=routing_key,
            body=json.dumps(data),
            properties=properties)

    def _check_event(self, event, data, routing_key, connection):
        self._check_send(event, data)
        self.assertEqual(connection().channel().basic_publish.mock_calls,
                         [self._publish_call(event, data, routing_key)])

    @mock.patch('pika.BlockingConnection')
    def test_job_event(self, connection):
        """Check routing of a job event."""
        self._check_event('Job Hook', {
            'object_kind': 'build',
            'repository': {
                'homepage': 'https://host.name:1234/group/subgroup/project',
            }
        }, 'host.name.group.subgroup.project.build', connection)

    @mock.patch('pika.BlockingConnection')
    def test_pipeline_event(self, connection):
        """Check routing of a pipeline event."""
        self._check_event('Pipeline Hook', {
            'object_kind': 'pipeline',
            'project': {
                'web_url': 'https://host.name:1234/group/subgroup/project',
            },
        }, 'host.name.group.subgroup.project.pipeline', connection)

    def test_entrypoint_missing(self):
        """Check a missing entrypoint."""
        response = self.client.post('/non-existent', json={})
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_websecret_missing(self):
        """Check a missing websecret."""
        response = self.client.post('/', json={})
        self.assertEqual(response.status, '403 FORBIDDEN')

    def test_websecret_wrong(self):
        """Check a wrong websecret."""
        response = self.client.post(
            '/', json={}, headers={'X-Gitlab-Token': 'bad_secret'})
        self.assertEqual(response.status, '403 FORBIDDEN')

    @mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': ''})
    def test_websecret_env_missing(self):
        """Check a missing websecret env variable."""
        response = self.client.post('/', json={})
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_missing_object_kind(self):
        """Check a missing object_kind."""
        response = self.client.post(
            '/', json={}, headers={'X-Gitlab-Token': SECRET})
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')

    @mock.patch('pika.BlockingConnection')
    def test_errors(self, connection):
        """Check transient error handling."""
        routing_key = 'host.group.subgroup.project.build'
        messages = [{
            'index': index,
            'object_kind': 'build',
            'repository': {
                'homepage': 'https://host:1234/group/subgroup/project',
            },
        } for index in range(3)]

        connection().channel().basic_publish.side_effect = [
            OSError('first'), OSError('second'), True, True, True]

        self._check_send('Job Hook', messages[0])
        self._check_send('Job Hook', messages[1])
        self.assertEqual(connection().channel().basic_publish.mock_calls, [
            self._publish_call('Job Hook', messages[0], routing_key)] * 2)

        connection().channel().basic_publish.mock_calls = []
        self._check_send('Job Hook', messages[2])
        self.assertEqual(connection().channel().basic_publish.mock_calls, [
            self._publish_call('Job Hook', messages[i], routing_key)
            for i in range(3)])


@mock.patch.dict(os.environ, {
    'WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET': SECRET,
    'IS_PRODUCTION': 'True'})
class TestSentry(unittest.TestCase):
    """Test Sentry webhook receiver."""

    def setUp(self):
        """SetUp class."""
        self.client = flask.app.test_client()

    def _check_send(self, resource, data):
        data = json.dumps(data).encode('utf-8')
        signature = hmac.new(
            key=SECRET.encode('utf-8'),
            msg=data,
            digestmod=hashlib.sha256,
        ).hexdigest()
        response = self.client.post(
            '/sentry',
            data=data, content_type='application/json',
            headers={'Sentry-Hook-Resource': resource,
                     'Sentry-Hook-Signature': signature})
        self.assertEqual(response.status, '200 OK')

    @staticmethod
    def _publish_call(resource, data, routing_key):
        properties = pika.BasicProperties(
            delivery_mode=2,
            headers={
                'webhook-type': 'sentry',
                'webhook-sentry-resource': resource})
        return mock.call(
            exchange=receiver.RABBITMQ_EXCHANGE,
            routing_key=routing_key,
            body=json.dumps(data),
            properties=properties)

    def _check_event(self, resource, data, routing_key, connection):
        self._check_send(resource, data)
        if routing_key:
            self.assertEqual(connection().channel().basic_publish.mock_calls, [
                self._publish_call(resource, data, routing_key)])
        else:
            self.assertFalse(connection().channel().basic_publish.mock_calls)

    @mock.patch('pika.BlockingConnection')
    def test_issue(self, connection):
        """Check routing of an issue event."""
        self._check_event('issue', {
            'action': 'created',
            'data': {'issue': {
                'project': {'slug': 'project-slug'}
            }}
        }, 'sentry.io.project-slug.issue.created', connection)

    @mock.patch('pika.BlockingConnection')
    def test_event(self, connection):
        """Check routing of an event alert."""
        self._check_event('event_alert', {
            'action': 'triggered',
            'data': {'event': {
                'url': 'https://sentry.io/api/0/projects/org/project/events/1/'
            }}
        }, 'sentry.io.project.event_alert.triggered', connection)

    @mock.patch('pika.BlockingConnection')
    def test_unknown(self, connection):
        """Check ignoring of an unknown event."""
        self._check_event('unknown', {},
                          None, connection)

    def test_websecret_missing(self):
        """Check a missing signature."""
        response = self.client.post('/sentry', json={})
        self.assertEqual(response.status, '403 FORBIDDEN')

    def test_websecret_wrong(self):
        """Check a wrong websecret."""
        response = self.client.post(
            '/sentry', json={}, headers={'Sentry-Hook-Signature': 'invalid'})
        self.assertEqual(response.status, '403 FORBIDDEN')

    @mock.patch.dict(os.environ,
                     {'WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET': ''})
    def test_websecret_env_missing(self):
        """Check a missing websecret env variable."""
        response = self.client.post('/sentry', json={})
        self.assertEqual(response.status, '404 NOT FOUND')
