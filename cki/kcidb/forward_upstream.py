"""Get messages from DW and forward them to upstream KCIDB."""
import argparse
import os

import kcidb
import prometheus_client as prometheus
import sentry_sdk

from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue

LOGGER = get_logger('cki.kcidb.forward_upstream')

IS_PRODUCTION = misc.is_production()

KCIDB_PROJECT_ID = os.environ.get('KCIDB_PROJECT_ID')
KCIDB_DATASET_NAME = os.environ.get('KCIDB_DATASET_NAME')
KCIDB_TOPIC_NAME = os.environ.get('KCIDB_TOPIC_NAME')

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST')
RABBITMQ_PORT = misc.get_env_int('RABBITMQ_PORT', 5672)
RABBITMQ_USER = os.environ.get('RABBITMQ_USER')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD')
RABBITMQ_EXCHANGE = os.environ.get('RABBITMQ_EXCHANGE')
RABBITMQ_QUEUE = os.environ.get('RABBITMQ_QUEUE')
RABBITMQ_TIMEOUT_S = misc.get_env_int('RABBITMQ_TIMEOUT_S', 30)

METRICS_ENABLED = misc.get_env_bool('CKI_METRICS_ENABLED', False)
METRICS_PORT = misc.get_env_int('CKI_METRICS_PORT', 8000)
METRIC_MESSAGE_PROCESS_TIME = prometheus.Summary(
    'message_process_seconds', 'Time spent processing a message')
METRIC_MESSAGE_RECEIVED = prometheus.Counter(
    'message_received', 'Number of queue messages received')
METRIC_MESSAGE_PROCESS = prometheus.Counter(
    'message_processed', 'Number of queue messages processed')
METRIC_MESSAGE_IGNORED = prometheus.Counter(
    'message_ignored', 'Number of queue messages ignored')
METRIC_OBJECT_BUFFERED = prometheus.Histogram(
    'objects_buffered', 'Number of KCIDB objects buffered for forwarding')
METRIC_OBJECT_FORWARDED = prometheus.Counter(
    'object_forwarded', 'Number of KCIDB objects forwarded')


class KCIDBMessage:

    def __init__(self, kcidb_client, dry_run=False):
        self.kcidb_client = kcidb_client
        self.dry_run = dry_run
        self.revisions = []
        self.builds = []
        self.tests = []
        self.version = None
        self.msg_ack_callbacks = []

    def add(self, obj_type, obj, ack_fn):
        """Add the object to the correct list."""
        LOGGER.debug('Adding to KCIDBMessage: "%s" (%s)', obj_type, obj)
        obj_lists = {
            'revision': self.revisions,
            'build': self.builds,
            'test': self.tests,
        }
        obj_lists[obj_type].append(obj)
        self.msg_ack_callbacks.append(ack_fn)

        METRIC_OBJECT_BUFFERED.observe(len(self))

        if not self.version:
            self.version = misc.get_nested_key(obj, 'misc/kcidb/version')

        for key in ('kcidb', 'is_public'):
            try:
                del obj['misc'][key]
            except KeyError:
                pass

    @property
    def encoded(self):
        """Return the KCIDB message object."""
        data = {
            'version': self.version,
            'revisions': self.revisions,
            'builds': self.builds,
            'tests': self.tests,
        }

        kcidb.io.schema.validate(data)

        return data

    @property
    def has_objects(self):
        """Return true if any of the lists has elements."""
        return len(self) > 0

    def clear(self):
        """Remove all elements."""
        LOGGER.debug('Clearing KCIDBMessage elements')
        self.revisions = []
        self.builds = []
        self.tests = []
        self.version = None
        self.msg_ack_callbacks = []

    def ack_messages(self):
        """Ack all messages contained on this object."""
        LOGGER.debug('Acking %i messages', len(self.msg_ack_callbacks))
        for ack_fn in self.msg_ack_callbacks:
            ack_fn()

    def submit(self):
        """Upload message to upstream kcidb."""
        LOGGER.debug('Submitting KCIDBMessage elements')
        if not self.dry_run:
            self.kcidb_client.submit(self.encoded)
            LOGGER.info('%i elements submitted', len(self))
            METRIC_OBJECT_FORWARDED.inc(amount=len(self))
        else:
            LOGGER.info('Skipping submit. Dry Run')
        self.ack_messages()
        self.clear()

    def __len__(self):
        """Return number of elements."""
        return (
            len(self.revisions) +
            len(self.builds) +
            len(self.tests)
        )


@METRIC_MESSAGE_PROCESS_TIME.time()
def callback(message, body, ack_fn):
    """Process one received message."""
    LOGGER.debug('Message Received: %s', body)

    if not body:
        if message.has_objects:
            LOGGER.debug('Calling message.submit')
            message.submit()
        return

    LOGGER.info('Got message for %s iid=%s', body['object_type'], body['iid'])
    METRIC_MESSAGE_RECEIVED.inc()

    status = body['status']
    if not status == 'new':
        LOGGER.info('Message status is not "new". Skipping (status: %s)', status)
        METRIC_MESSAGE_IGNORED.inc()
        ack_fn()
        return

    is_public = misc.get_nested_key(body, 'object/misc/is_public')
    if not is_public:
        LOGGER.info('Message is not public. Skipping (is_public: %s)', is_public)
        METRIC_MESSAGE_IGNORED.inc()
        ack_fn()
        return

    message.add(body['object_type'], body['object'], ack_fn)
    METRIC_MESSAGE_PROCESS.inc()


def process_queue(args):
    """Listen for Data Warehouse KCIDB messages."""
    queue = MessageQueue(
        args.rabbitmq_host, args.rabbitmq_port,
        args.rabbitmq_user, args.rabbitmq_password,
    )

    kcidb_client = kcidb.Client(
        project_id=args.kcidb_project_id,
        dataset_name=args.kcidb_dataset_name,
        topic_name=args.kcidb_topic_name
    )

    message = KCIDBMessage(kcidb_client, dry_run=not IS_PRODUCTION)

    queue.consume_messages(args.rabbitmq_exchange, ['#'],
                           lambda _, body, ack: callback(message, body, ack),
                           queue_name=args.rabbitmq_queue,
                           prefetch_count=0,
                           inactivity_timeout=args.rabbitmq_timeout_s,
                           return_on_timeout=False,
                           manual_ack=True)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='Parse a Pipeline or Job and get KCIDB schemed data.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--metrics-enable', action='store_true', default=METRICS_ENABLED,
                        help='Enable exposing prometheus metrics.')
    parser.add_argument('--metrics-port', default=METRICS_PORT, type=int,
                        help='Port to use for serving prometheus metrics.')

    parser.add_argument('--rabbitmq-exchange', default=RABBITMQ_EXCHANGE,
                        help='Defaults to env RABBITMQ_EXCHANGE.')
    parser.add_argument('--rabbitmq-queue', default=RABBITMQ_QUEUE,
                        help='Defaults to env RABBITMQ_QUEUE.')
    parser.add_argument('--rabbitmq-host', default=RABBITMQ_HOST,
                        help='Defaults to env RABBITMQ_HOST.')
    parser.add_argument('--rabbitmq-port', type=int, default=RABBITMQ_PORT,
                        help='Defaults to env RABBITMQ_PORT.')
    parser.add_argument('--rabbitmq-user', default=RABBITMQ_USER,
                        help='Defaults to env RABBITMQ_USER.')
    parser.add_argument('--rabbitmq-password', default=RABBITMQ_PASSWORD,
                        help='Defaults to env RABBITMQ_PASSWORD.')
    parser.add_argument('--rabbitmq-timeout-s', default=RABBITMQ_TIMEOUT_S,
                        help='Defaults to env RABBITMQ_TIMEOUT_S.')

    parser.add_argument('--kcidb-project-id', default=KCIDB_PROJECT_ID,
                        help='Defaults to env KCIDB_PROJECT_ID.')
    parser.add_argument('--kcidb-dataset-name', default=KCIDB_DATASET_NAME,
                        help='Defaults to env KCIDB_DATASET_NAME.')
    parser.add_argument('--kcidb-topic-name', default=KCIDB_TOPIC_NAME,
                        help='Defaults to env KCIDB_TOPIC_NAME.')

    return parser.parse_args()


def main():
    """CLI Interface."""
    args = parse_args()

    if misc.is_production():
        sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))

    if args.metrics_enable:
        prometheus.start_http_server(args.metrics_port)

    process_queue(args)


if __name__ == '__main__':
    main()
