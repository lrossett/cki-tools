"""Backup and restore grafana dashboards and config."""
import argparse
import pathlib

from . import objects


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='Helper for Grafana config version controlling.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('action', help='Action to perform',
                        choices=('upload', 'download'))
    parser.add_argument('--path', help='Path to save the data in',
                        default='data/')

    return parser.parse_args()


def main():
    """CLI Interface."""
    args = parse_args()
    path = pathlib.Path(args.path).resolve()

    for obj_class in objects.ALL_OBJECTS:
        obj = obj_class(path)

        if args.action == 'download':
            obj.delete_all()
            obj.download_all()
        elif args.action == 'upload':
            obj.upload_all()


if __name__ == '__main__':
    main()
