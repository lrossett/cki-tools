"""Grafana objects."""
import os
import json
import pathlib
import shutil

from requests.exceptions import HTTPError
from cki_lib import logger, session

LOGGER = logger.get_logger('deployment_tools.grafana')
SESSION = session.get_session('deployment_tools.grafana', LOGGER)

GRAFANA_HOST = os.environ['GRAFANA_HOST']
GRAFANA_TOKEN = os.environ['GRAFANA_TOKEN']


class API:
    # pylint: disable=too-few-public-methods
    """API requests."""

    headers = {
        'Authorization': f'Bearer {GRAFANA_TOKEN}',
        'Content-Type': 'application/json',
    }

    def _get(self, endpoint):
        """GET Request."""
        return self._request('get', endpoint)

    def _post(self, endpoint, data):
        """POST Request."""
        return self._request('post', endpoint, data)

    def _put(self, endpoint, data):
        """PUT Request."""
        return self._request('put', endpoint, data)

    def _request(self, method, endpoint, data=None):
        """Perform request."""
        url = f'{GRAFANA_HOST}{endpoint}'
        LOGGER.debug('%s: %s (data=%s)', method, url, data)
        response = getattr(SESSION, method)(
            url, data=json.dumps(data), headers=self.headers
        )
        response.raise_for_status()
        return response.json()


class Object(API):
    """Generic Grafana object."""

    _name = None
    _uid = None

    _url = None
    _identifier = None

    def __init__(self, path):
        """Init."""
        self.path = pathlib.Path(path, self._name)

    def _save(self, data):
        """Save the object to the filesystem."""
        file_name = f'{data[self._uid]}.json'.replace(' ', '_')
        file_path = pathlib.Path(self.path, file_name)
        file_path.parent.mkdir(parents=True, exist_ok=True)

        LOGGER.info(
            '%s: Saving: %s', self._name, file_path
        )
        file_path.write_text(
            json.dumps(data, indent=4, sort_keys=True)
        )

    def _get_element(self, element):
        # pylint: disable=no-self-use
        """Perform some action with element if necessary."""
        return element

    def _remote_exists(self, obj):
        """Return True if the object exists on the server."""
        endpoint = self._url + self._identifier.format(**obj)
        try:
            self._get(endpoint)
            return True
        except HTTPError:
            return False

    def _remote_update(self, obj):
        """Update remote copy of this object."""
        LOGGER.info(
            '%s: Updating %s', self._name, obj[self._uid]
        )

        endpoint = self._url + self._identifier.format(**obj)
        self._put(endpoint, obj)

    def _remote_create(self, obj):
        """Create new object in the server."""
        LOGGER.info(
            '%s: Creating %s', self._name, obj[self._uid]
        )

        self._post(self._url, obj)

    def delete_all(self):
        """Delete all local storage."""
        LOGGER.info(
            '%s: Deleting local copy: %s', self._name, self.path
        )
        if self.path.is_dir():
            shutil.rmtree(self.path)

    def download_all(self):
        """Download and locally store all objects."""
        LOGGER.info(
            '%s: Downloading all to: %s', self._name, self.path
        )

        all_elements = self._get(self._url)
        for element in all_elements:
            element = self._get_element(element)
            self._save(element)

    def upload_all(self):
        """Update if exists, otherwise create."""
        files = pathlib.Path(self.path).glob('*.json')
        for file in files:
            obj = json.loads(file.read_text())
            if self._remote_exists(obj):
                self._remote_update(obj)
            else:
                self._remote_create(obj)


class NotificationChannel(Object):
    """NotificationChannel."""

    _name = 'notification_channel'
    _uid = 'uid'

    _url = '/api/alert-notifications'
    _identifier = '/uid/{uid}'


class Datasource(Object):
    """Datasource."""

    _name = 'datasource'
    _uid = 'id'

    _url = '/api/datasources'
    _identifier = '/{id}'


class Dashboard(Object):
    """Dashboard."""

    _name = 'dashboard'
    _uid = 'uid'
    _url = '/api/search'

    def _get_element(self, element):
        endpoint = f'/api/dashboards/uid/{element["uid"]}'
        return self._get(endpoint)['dashboard']

    @staticmethod
    def _remote_exists(_):
        """
        Return True.

        Dashboards needs to be always updated, no matter if they exist or not.
        """
        return True

    def _remote_update(self, obj):
        """Update or create dashboard."""
        LOGGER.info(
            '%s: Updating %s', self._name, obj['title']
        )

        # Delete ID so it creates a new one if not present.
        del obj['id']

        data = {
            'dashboard': obj,
            'overwrite': True,
        }
        self._post('/api/dashboards/db', data)


ALL_OBJECTS = (
    NotificationChannel,
    Datasource,
    Dashboard
)
