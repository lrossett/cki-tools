"""Render jinja2 templates."""
import argparse
import os
import pathlib
import re

import yaml
import jinja2


@jinja2.contextfilter
def from_env(_, value):
    """Try to get a variable from env if the value matches ${*}."""
    if not isinstance(value, str):
        return value

    # Try to match ${*} with or without brackets.
    return re.sub(
        r'\$(\w+|\{([^}]*)\})',
        lambda m: os.environ[m.group(2) or m.group(1)],
        value
    )


def render(template_path, data_path):
    """Render and return the template."""
    template_file = pathlib.Path(template_path)
    data_file = pathlib.Path(data_path)

    data = yaml.safe_load(data_file.read_text())
    loader = jinja2.FileSystemLoader(template_file.parent)

    env = jinja2.Environment(loader=loader)
    env.filters['env'] = from_env

    template = env.get_template(template_file.name)
    return template.render(data, env=os.environ)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='Render a template with jinja2',
    )
    parser.add_argument('template', help="Template file to render.")
    parser.add_argument('data', help="Data JSON/YAML file for the template.")
    parser.add_argument('-o', '--output',
                        help="Output path for the rendered file.")
    return parser.parse_args()


def main():
    """Parse the template from CLI."""
    args = parse_args()

    rendered = render(args.template, args.data)

    if args.output:
        output_file = pathlib.Path(args.output)
        output_file.write_text(rendered)
    else:
        print(rendered)


if __name__ == '__main__':
    main()
