#!/bin/bash
set -euo pipefail

# Check the number of jobs in a Beaker queue
#
# Usage: beaker-queue-check.sh QUEUE_NAME USER_NAME MAX_QUEUE_COUNT
#
# Required environment variables:
# - BEAKER_STATS_URL

QUEUES="$(curl -s "${BEAKER_STATS_URL}/top-recipe-owners.$1")"
QUEUE_LINE="$(egrep "^$2" <<< "${QUEUES}" || true)"

if [ -z "${QUEUE_LINE}" ]; then
    echo "No jobs queued"
    exit 0
fi

JOB_COUNT="$(awk '{print $NF}' <<< "${QUEUE_LINE}")"
echo "${JOB_COUNT} jobs queued"

test "${JOB_COUNT}" -le "$3"
