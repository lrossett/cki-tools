#!/bin/bash
set -euo pipefail

# Notify the IRC channel
#
# Usage: irc-notification.sh
#
# Required environment variables:
# - IRCBOT_URL

if [[ ${MONIT_DESCRIPTION} =~ failed|'limit matched' ]]; then
  EMOJI="🔴"
else
  EMOJI="🔵"
fi

MESSAGE="${EMOJI} [${MONIT_SERVICE}] ${MONIT_DESCRIPTION}"
if [ -n "${IRCBOT_URL}" ]; then
    http --ignore-stdin POST "${IRCBOT_URL}" "message=${MESSAGE}"
fi
