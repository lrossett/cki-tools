#!/bin/bash
set -euo pipefail

# Check spec file requirements for changes
#
# Usage: specfile-requirements-check.sh SPECFILE_URL EXPECTED_CHECKSUM

CHECKSUM="$(curl -kLs --retry 3 "$1" | \
    egrep "^(Build)?Requires" | sha256sum | cut -d' ' -f 1)"

if [ "${CHECKSUM}" = "$2" ]; then
  echo "Build requirements unchanged."
  exit 0
else
  echo "Build requirements changed, expected $2, got ${CHECKSUM}!"
  exit 1
fi
